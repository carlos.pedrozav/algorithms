package unionfind;

import java.util.*;
import java.io.*;
import unionfind.*;

public class UnionFind {
    public static void main(String[] args) {
        System.out.println("INIT"); 
        int N = StdIn.readInt();   
        System.out.println("N: " + N ); 
        QuickFindUF uf = new QuickFindUF(N);   
        while (!StdIn.isEmpty())   {      
            int p = StdIn.readInt();      
            int q = StdIn.readInt();      
            if (!uf.connected(p, q))      
            {         
                uf.union(p, q);         
                System.out.println("Conect: " + p + " to " + q);    
           } 
       }
   }
}
